import numpy as np
import torch
import torch.nn as nn


class SelfAttention(nn.Module):

    def __init__(self, nhid, attn_units, attn_hops, dropout=0.1):
        super(SelfAttention, self).__init__()
        self.drop = nn.Dropout(dropout)
        self.ws1 = nn.Linear(nhid * 2, attn_units, bias=False)
        self.ws2 = nn.Linear(attn_units, attn_hops, bias=False)
        self.tanh = nn.Tanh()
        self.relu = nn.ReLU()
        self.softmax = nn.Softmax(dim=1)
        #        self.init_weights()
        self.attention_hops = attn_hops

    def forward(self, inp, mask=None):
        size = inp.size()  # [bsz, len, nhid*2]
        compressed_embeddings = inp.contiguous().view(-1, size[2])  # [bsz*len, nhid*2]

        hbar = self.relu(self.ws1(self.drop(compressed_embeddings)))  # [bsz*len, attn_units]
        alphas = self.ws2(hbar).view(size[0], size[1], -1)  # [bsz, len, hop]
        alphas = torch.transpose(alphas, 1, 2).contiguous()  # [bsz, hop, len]
        if mask is not None:
            alphas = alphas.masked_fill(~mask.unsqueeze(1).byte(), -np.inf)
        alphas = self.softmax(alphas.view(-1, size[1]))  # [bsz*hop, len]
        alphas = alphas.view(size[0], self.attention_hops, size[1])  # [bsz, hop, len]
        return torch.bmm(alphas, inp), alphas
