#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
import torch.nn as nn
from uawa_att import SelfAttention


class XfModel(nn.Module):
    def __init__(self, hidden_size=128):
        super(XfModel, self).__init__()
        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv2 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv3 = nn.Sequential(
            nn.Conv2d(64, 64, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.conv4 = nn.Sequential(
            nn.Conv2d(64, 256, kernel_size=(3, 3), padding=1),
            nn.BatchNorm2d(256),
            nn.ReLU(),
            nn.MaxPool2d(2, padding=1))
        self.dropout = nn.Dropout(0.5)
        # LSTM(特征尺度，隐藏层），输入是[batch,时序，特征]，输出是[batch,len，hidden_size*2]
        self.hidden_size = hidden_size
        self.lstm = nn.LSTM(256, self.hidden_size, batch_first=True, bidirectional=True)
        self.dnn = nn.Linear(self.hidden_size * 2, 4)

    def forward(self, x, lengths):
        lengths = lengths.clone().detach()
        x = x.view(x.shape[0], 1, x.shape[1], x.shape[2])
        out = self.conv1(x)
        lengths = lengths // 2 + 1
        out = self.conv2(out)
        lengths = lengths // 2 + 1
        out = self.conv3(out)
        lengths = lengths // 2 + 1
        out = self.conv4(out)

        out = self.dropout(out)

        lengths = lengths // 2 + 1
        lengths = lengths

        out = torch.nn.functional.adaptive_avg_pool2d(out, (1, out.size()[3]))
        # 把通道和特征维度相乘，LSTM的输入是（batch，时序，特征），之后交换特征和时序位置
        out = out.view(out.size()[0], out.size()[1] * out.size()[2], -1)
        out = out.transpose(1, 2).contiguous()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, lengths.cpu(), batch_first=True)

        out, (hn, cn) = self.lstm(out)

        out, _ = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        # average over lstm output, ignoring zero-padding part
        mask = (torch.arange(lengths[0]).cuda()[None, :] < lengths[:, None]).float()
        res = out * mask.unsqueeze(-1)  # res: (batch, lengths[0], hidden_size)
        res = res.sum(dim=1)  # res:(batch, hidden_size)
        res = res / (lengths.unsqueeze(-1))  # res:(batch, hidden_size)
        out = self.dnn(res)
        return out, 0
